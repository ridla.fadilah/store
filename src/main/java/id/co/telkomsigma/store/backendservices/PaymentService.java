package id.co.telkomsigma.store.backendservices;

import java.util.Map;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author <a href="mailto:ridla.fadilah@gmail.com">Ridla Fadilah</a>
 */
@FeignClient(value="payment")
public interface PaymentService {
	
	 @RequestMapping(method = RequestMethod.GET, value = "/api/payment/{harga}", consumes = "application/json")
	    public Map<String, Object>  cariProdukById(@PathVariable("harga") String harga);

}
