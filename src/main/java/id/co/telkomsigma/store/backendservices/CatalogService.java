package id.co.telkomsigma.store.backendservices;

import java.util.ArrayList;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import id.co.telkomsigma.store.dto.Product;
import id.co.telkomsigma.store.dto.ProductPhotos;

/**
 * @author <a href="mailto:ridla.fadilah@gmail.com">Ridla Fadilah</a>
 */
@FeignClient(value="catalog", fallback = CatalogService.CatalogFallback.class)
public interface CatalogService {
    @RequestMapping(method = RequestMethod.GET, value = "/api/product/{id}", consumes = "application/json")
    public Product cariProdukById(@PathVariable("id") String id);

    @RequestMapping(method = RequestMethod.GET, value = "/api/product/{id}/photos", consumes = "application/json")
    public Iterable<ProductPhotos> fotoProdukById(@PathVariable("id") String id);
    
    static class CatalogFallback implements CatalogService{
        @Override
        public Product cariProdukById(String id) {
            return null;
        }

        @Override
        public Iterable<ProductPhotos> fotoProdukById(String id) {
            return new ArrayList<>();
        }
    }

}
