package id.co.telkomsigma.store.dto;

import lombok.Data;

/**
 * @author <a href="mailto:ridla.fadilah@gmail.com">Ridla Fadilah</a>
 */
@Data
public class ProductPhotos {
    private String id;
    private Product product;
    private String url;    
}
