package id.co.telkomsigma.store.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Data;

/**
 * @author <a href="mailto:ridla.fadilah@gmail.com">Ridla Fadilah</a>
 */
@Data
public class PurchaseOrder {
    private Date waktuTransaksi = new Date();
    private List<CartItem> daftarBelanja = new ArrayList<>();
    private Shipment pengiriman = new Shipment();
}
