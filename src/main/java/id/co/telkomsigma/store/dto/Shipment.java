package id.co.telkomsigma.store.dto;

import java.math.BigDecimal;

import lombok.Data;

/**
 * @author <a href="mailto:ridla.fadilah@gmail.com">Ridla Fadilah</a>
 */

@Data
public class Shipment {
    private String provider;
    private String jenis;
    private String asal;
    private String tujuan;
    private BigDecimal berat;
    private BigDecimal biaya;
}
