package id.co.telkomsigma.store.dto;

import java.math.BigDecimal;

import lombok.Data;

/**
 * @author <a href="mailto:ridla.fadilah@gmail.com">Ridla Fadilah</a>
 */
@Data
public class Product {
    private String id;
    private String code;
    private String name;
    private BigDecimal weight;
    private BigDecimal price;
}
