package id.co.telkomsigma.store.dto;

import lombok.Data;

/**
 * @author <a href="mailto:ridla.fadilah@gmail.com">Ridla Fadilah</a>
 */
@Data
public class CartItem {
    private Product product;
    private Integer jumlah;    
}
